import { createRouter, createWebHistory } from 'vue-router';
import Layout from '../Layout/Layout.vue';
import HomePage from '../HomePage/HomePage.vue';
import LoginPage from '../Page/LoginPage/LoginPage.vue';

const routes = [
    {
        path: '/',
        component: Layout,
        props: {
            Component: HomePage,
        }
    },
    {
        path: '/login',
        component: Layout,
        props: {
            Component: LoginPage,
        }
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
})

export default router;
