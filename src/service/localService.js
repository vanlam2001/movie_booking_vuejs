export const USER_INFO = "USER_INFO";
export const localUserServ = {
    get: () => {
        let jsonData = localStorage.getItem('USER_INFO');
        if (jsonData != "" && jsonData != "undifined") {
            return JSON.parse(jsonData) ? JSON.parse(jsonData) : null;
        }
    },
    set: (userInfo) => {
        let jsonData = userInfo ? JSON.stringify(userInfo) : "";
        localStorage.setItem(USER_INFO, jsonData);
    },
    remove: () => {
        localStorage.removeItem(USER_INFO);
    }
}

